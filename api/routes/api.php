<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
// Buyer
Route::namespace('Buyer')->group(function () {
    Route::resource('buyers', 'BuyerController', ['only' => ['index', 'show']]);
});

// Category
Route::namespace('Category')->group(function () {
    Route::resource('categories', 'CategoryController', ['except' => ['create', 'edit']]);
});

// Product
Route::namespace('Product')->group(function () {
    Route::resource('products', 'ProductController', ['only' => ['index', 'show']]);
});

// Transaction
Route::namespace('Transaction')->group(function () {
    Route::resource('transactions', 'TransactionController', ['only' => ['index', 'show']]);
});

// Seller
Route::namespace('Seller')->group(function () {
    Route::resource('sellers', 'SellerController', ['only' => ['index', 'show']]);
});

// User
Route::namespace('User')->group(function () {
    Route::resource('users', 'UserController', ['except' => ['create', 'edit']]);
});
