<?php

use App\User;
use App\Buyer;
use App\Seller;
use App\Product;
use App\Category;
use App\Transaction;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

/*
 * El token de verificacion solamente deberia generarse si el usuario NO esta verificado.
 */
$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'), // secret
        'remember_token' => str_random(10),
        'verified' => $verified = $faker->randomElement([User::USUARIO_VERIFICADO, User::USUARIO_NO_VERIFICADO]),
        'verification_token' => $verified == User::USUARIO_VERIFICADO ? null : User::makeVerificationToken(),
        'admin' => $faker->randomElement([User::USUARIO_ADMINISTRADOR, User::USUARIO_REGULAR]),
    ];
});

$factory->define(Category::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph,
    ];
});

/*
 * Formas de obtener el id de un seller(User)
 * User::inRandomOrder()->first()->id
 * User::inRandomOrder('id')->first()->id
 * User::select('id')->inRandomOrder()
 * User::all()->random()->id
 *
 */
$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->paragraph,
        'quantity' => $faker->numberBetween(1, 10),
        'status' => $faker->randomElement([Product::PRODUCTO_DISPONIBLE, Product::PRODUCTO_NO_DISPONIBLE]),
        'image' => $faker->randomElement(['001.jpg', '002.jpg', '003.jpg']),
        'seller_id' => User::all()->random()->id
    ];
});

/*
 * Un comprador no puede comprar su mismo(propio) producto.
 * $seller es un vendedor cualquiera (se sabe que un usuario es un vendedor, cuando este usuario ya tenga un producto/s)
 * $buyer es un comprador. puede ser cualquier usuario pero con la condicion que de no sea el mismo id del seller.
 */
$factory->define(Transaction::class, function (Faker $faker) {
    $seller = Seller::has('products')->get()->random();
    $buyer = User::all()->except($seller->id)->random();
    return [
        'quantity' => $faker->numberBetween(1, 3),
        'buyer_id' => $buyer->id,
        'product_id' => $seller->products->random()->id,
    ];
});
