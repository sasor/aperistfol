<?php

use App\User;
use App\Product;
use App\Category;
use App\Transaction;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0'); // desactiva la verificacion de llaves foraneas.
        User::truncate();
        Category::truncate();
        Product::truncate();
        Transaction::truncate();
        DB::table('category_product')->truncate();

        $users = 2000;
        $categories = 30;
        $products = 1000;
        $transactions = 1000;

        // importa el orden en que se ejecutan los factories.
        // ejemplo: Product antes que Transaction. Category antes que Product.
        factory(User::class, $users)->create();
        factory(Category::class, $categories)->create();
        factory(Product::class, $products)->create()->each(
            function ($product) {
                $categories = Category::all()->random(mt_rand(1, 5))->pluck('id');
                $product->categories()->attach($categories);
            }
        );
        factory(Transaction::class, $transactions)->create();
    }
}
