<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public const PRODUCTO_DISPONIBLE = 'disponible';
    public const PRODUCTO_NO_DISPONIBLE = 'no disponible';

    protected $fillable = [
        'name',
        'description',
        'quantity',
        'status',
        'seller_id'
    ];

    public function isAvailable()
    {
        return $this->status === Product::PRODUCTO_DISPONIBLE;
    }

    public function seller()
    {
        return $this->belongsTo(Seller::class);
    }

    public function transactions()
    {
        return $this->hasMany(Transaction::class);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
