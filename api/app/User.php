<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    public const USUARIO_VERIFICADO = '1';
    public const USUARIO_NO_VERIFICADO = '0';

    public const USUARIO_ADMINISTRADOR = 'true';
    public const USUARIO_REGULAR = 'false';

    // definido explicitamente debido al error que tira cuando ejecutas los seeders.
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'verification_token'
    ];

    public function isVerified()
    {
        return $this->verified == User::USUARIO_VERIFICADO;
    }

    public function isAdministrator()
    {
        return $this->admin == User::USUARIO_ADMINISTRADOR;
    }

    public static function makeVerificationToken()
    {
        return str_random(60);
    }
}
